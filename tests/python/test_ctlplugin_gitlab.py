# Copyright (c) 2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""get qualitygate gitlab extension unit tests."""

import unittest

from unittest.mock import MagicMock, patch

from opentf.tools.ctlplugin_gitlab import (
    publish_results,
    _maybe_get_noteid,
    _format_qualitygate_response,
    dispatch_results_by_target_type,
    set_label,
)

from requests import HTTPError, Response

########################################################################
# Datasets

GL_PARAMS = {
    'server': 'https://gl.gl',
    'project': '12345678',
    'target': 'merge_requests',
    'target_id': '1',
    'token': 'glpat-X-abcDefGhij',
    'keep_history': 'true',
}

QG_RESPONSE_SUCCESS = {
    'status': 'SUCCESS',
    'rules': {
        'rule A': {
            'result': 'SUCCESS',
            'scope': 'true',
            'success_ratio': '99.1%',
            'tests_failed': '1',
            'tests_in_scope': '101',
            'tests_passed': '100',
            'threshold': '90%',
        }
    },
}

QG_RESPONSE_FAILURE = {
    'status': 'FAILURE',
    'rules': {
        'rule B': {
            'result': 'FAILURE',
            'scope': 'true',
            'success_ratio': '99.1%',
            'tests_failed': '1',
            'tests_in_scope': '101',
            'tests_passed': '100',
            'threshold': '100%',
        }
    },
    'warnings': ['Keep your eyes open'],
}

QG_RESPONSE_NOTEST = {
    'status': 'NOTEST',
    'rules': {'rule C': {'result': 'NOTEST', 'scope': 'true', 'threshold': '100%'}},
}

########################################################################
# Helpers

mockresponse = Response()
mockresponse.status_code = 200


def _make_response(status_code, json_value):
    mock = Response()
    mock.status_code = status_code
    mock.json = lambda: json_value  # pyright: ignore
    return mock


########################################################################


class TestCtlPluginGitlab(unittest.TestCase):
    def test_publish_results_ok(self):
        mock_pmn = MagicMock(return_value=({'id': '123'}, 201))
        mock_ahn = MagicMock(return_value=None)

        with patch('opentf.tools.ctlplugin_gitlab._post_note', mock_pmn), patch(
            'opentf.tools.ctlplugin_gitlab._maybe_get_noteid', mock_ahn
        ):
            publish_results(GL_PARAMS, 'mode', QG_RESPONSE_SUCCESS)
        mock_pmn.assert_called_once()
        self.assertTrue(mock_pmn.call_args[0][0].startswith(GL_PARAMS['server']))
        self.assertEqual(GL_PARAMS['project'], mock_pmn.call_args[0][0].split('/')[6])
        self.assertEqual(GL_PARAMS['target_id'], mock_pmn.call_args[0][0].split('/')[8])
        self.assertEqual(GL_PARAMS['token'], mock_pmn.call_args[0][-1]['private_token'])
        self.assertIn(
            'Quality gate status for mode mode', mock_pmn.call_args[0][1]['body']
        )
        mock_ahn.assert_not_called()

    def test_publish_results_without_token(self):
        mock_pmn = MagicMock(return_value=({'id': '345'}, 201))
        mock_ahn = MagicMock(return_value=None)
        params = GL_PARAMS.copy()
        del params['token']
        with patch('opentf.tools.ctlplugin_gitlab._post_note', mock_pmn), patch(
            'opentf.tools.ctlplugin_gitlab._maybe_get_noteid', mock_ahn
        ):
            publish_results(params, 'mode', QG_RESPONSE_SUCCESS)
        mock_pmn.assert_called_once()
        mock_ahn.assert_not_called()
        self.assertIsNone(mock_pmn.call_args[0][-1])

    def test_publish_results_with_cats(self):
        mock_pmn = MagicMock(return_value=({'id': '345'}, 201))
        mock_fqgr = MagicMock()
        mock_ahn = MagicMock(return_value=None)
        params = GL_PARAMS.copy()
        params['cats'] = 'true'
        with patch('opentf.tools.ctlplugin_gitlab._post_note', mock_pmn), patch(
            'opentf.tools.ctlplugin_gitlab._maybe_get_noteid', mock_ahn
        ), patch(
            'opentf.tools.ctlplugin_gitlab._format_qualitygate_response', mock_fqgr
        ):
            publish_results(params, 'mode', QG_RESPONSE_SUCCESS)
        mock_pmn.assert_called_once()
        mock_ahn.assert_not_called()
        mock_fqgr.assert_called_once_with('mode', QG_RESPONSE_SUCCESS, True)

    def test_publish_results_invalid_param(self):
        mock_error = MagicMock()
        with patch('opentf.tools.ctlplugin_gitlab._error', mock_error):
            publish_results({'project': 'b'}, 'mode', QG_RESPONSE_SUCCESS)
        mock_error.assert_called_once()
        self.assertIn(
            'Cannot post results to GitLab, missing mandatory parameters',
            mock_error.call_args[0][0],
        )
        self.assertEqual(
            'server, target, target-id, keep-history', mock_error.call_args[0][1]
        )

    def test_publish_results_aborting_no_mr(self):
        with patch('builtins.print') as mock_print:
            publish_results({'server': 'a'}, 'mode', {})
        mock_print.assert_called_once_with('Aborting publication, no MR to publish to.')

    def test_publish_results_invalid_keep_history(self):
        mock_error = MagicMock()
        with patch('opentf.tools.ctlplugin_gitlab._error', mock_error):
            publish_results(
                {
                    'server': 'a',
                    'project': 'b',
                    'target': 'issues',
                    'target_id': '01210',
                    'keep_history': 'brrrghds',
                },
                'mode',
                QG_RESPONSE_SUCCESS,
            )
        mock_error.assert_called_once_with(
            'Cannot post results to GitLab: `keep-history` parameter must be `true` or `false`, got %s.',
            'brrrghds',
        )

    def test_publish_results_bad_api_response(self):
        mockresponse = _make_response(400, {'error': 'Bad request'})
        mock_post = MagicMock(return_value=mockresponse)
        mock_ahn = MagicMock()
        mock_warning = MagicMock()
        with patch('requests.post', mock_post), patch(
            'opentf.tools.ctlplugin_gitlab._maybe_get_noteid', mock_ahn
        ), patch('opentf.tools.ctlplugin_gitlab._warning', mock_warning):
            publish_results(GL_PARAMS, 'mode', QG_RESPONSE_SUCCESS)
        mock_post.assert_called_once()
        mock_ahn.assert_not_called()
        mock_warning.assert_called_once()
        self.assertIn(
            'Failed to post Quality gate results:', mock_warning.call_args[0][0]
        )
        self.assertEqual(400, mock_warning.call_args[0][1])
        self.assertEqual({'error': 'Bad request'}, mock_warning.call_args[0][2])

    def test_publish_results_bad_api_response_not_json(self):
        mockresponse = Response()
        mockresponse.status_code = 422
        mock_post = MagicMock(return_value=mockresponse)
        mock_ahn = MagicMock()
        mock_error = MagicMock()
        with patch('requests.post', mock_post), patch(
            'opentf.tools.ctlplugin_gitlab._maybe_get_noteid', mock_ahn
        ), patch('opentf.tools.ctlplugin_gitlab._error', mock_error):
            publish_results(GL_PARAMS, 'mode', QG_RESPONSE_SUCCESS)
        mock_post.assert_called_once()
        mock_ahn.assert_not_called()
        mock_error.assert_called_once()
        self.assertIn(
            'Failed to post Quality gate results:', mock_error.call_args[0][0]
        )
        self.assertEqual(422, mock_error.call_args[0][1])
        self.assertEqual(
            'https://gl.gl/api/v4/projects/12345678/merge_requests/1/notes',
            mock_error.call_args[0][2],
        )

    def test_publish_results_api_exception(self):
        mock_post = MagicMock(side_effect=Exception('Failed'))
        mock_error = MagicMock()
        with patch('requests.post', mock_post), patch(
            'opentf.tools.ctlplugin_gitlab._error', mock_error
        ):
            publish_results(GL_PARAMS, 'mode', QG_RESPONSE_FAILURE)
        mock_post.assert_called_once()
        mock_error.assert_called_once()
        self.assertEqual('Failed', mock_error.call_args[0][1])

    def test_publish_results_api_exception_with_token(self):
        mock_post = MagicMock(side_effect=Exception('Failed glpat-X-abcDefGhij'))
        mock_error = MagicMock()
        with patch('requests.post', mock_post), patch(
            'opentf.tools.ctlplugin_gitlab._error', mock_error
        ):
            publish_results(GL_PARAMS, 'mode', QG_RESPONSE_FAILURE)
        mock_post.assert_called_once()
        mock_error.assert_called_once()
        self.assertEqual('Failed [MASKED]', mock_error.call_args[0][1])

    def test_publish_results_with_labels(self):
        mock_pmn = MagicMock(return_value=({'id': '345'}, 201))
        mock_ahn = MagicMock(return_value=None)
        mock_label = MagicMock()
        params = GL_PARAMS.copy()
        params['label'] = 'MyPreciousQualityGate'
        with patch('opentf.tools.ctlplugin_gitlab._post_note', mock_pmn), patch(
            'opentf.tools.ctlplugin_gitlab._maybe_get_noteid', mock_ahn
        ), patch('opentf.tools.ctlplugin_gitlab.set_label', mock_label):
            publish_results(params, 'mode', QG_RESPONSE_SUCCESS)
        mock_label.assert_called_once()
        args = mock_label.call_args[0]
        self.assertEqual(params['label'], args[1])
        self.assertEqual('SUCCESS', args[2])

    def test_publish_results_with_default_prefix(self):
        mock_pmn = MagicMock(return_value=({'id': '345'}, 201))
        mock_ahn = MagicMock(return_value=None)
        mock_label = MagicMock()
        params = GL_PARAMS.copy()
        params['label'] = 'default'
        with patch('opentf.tools.ctlplugin_gitlab._post_note', mock_pmn), patch(
            'opentf.tools.ctlplugin_gitlab._maybe_get_noteid', mock_ahn
        ), patch('opentf.tools.ctlplugin_gitlab.set_label', mock_label):
            publish_results(params, 'mode', QG_RESPONSE_FAILURE)
        mock_label.assert_called_once()
        args = mock_label.call_args[0]
        self.assertEqual('QualityGate', args[1])
        self.assertEqual('FAILURE', args[2])

    def test_update_results_ok(self):
        mock_pmn = MagicMock(return_value=({'id': '123'}, 201))
        mock_ahn = MagicMock(return_value='42')
        mock_dmn = MagicMock(return_value=204)
        params = GL_PARAMS.copy()
        params['keep_history'] = 'false'
        with patch('opentf.tools.ctlplugin_gitlab._post_note', mock_pmn), patch(
            'opentf.tools.ctlplugin_gitlab._maybe_get_noteid', mock_ahn
        ), patch('opentf.tools.ctlplugin_gitlab._delete_note', mock_dmn):
            publish_results(params, 'mode', QG_RESPONSE_SUCCESS)
        mock_pmn.assert_called_once()
        mock_ahn.assert_called_once()
        mock_dmn.assert_called_once()
        self.assertEqual('123', mock_ahn.call_args[0][-2])
        self.assertEqual('42', mock_dmn.call_args[0][0].split('/')[-1])

    def test_update_results_failed_to_delete_old(self):
        mock_pmn = MagicMock(return_value=({'id': '123'}, 201))
        mock_ahn = MagicMock(return_value='42')
        mockresponse = _make_response(401, {})
        mock_delete = MagicMock(return_value=mockresponse)
        mock_warning = MagicMock()
        params = GL_PARAMS.copy()
        params['keep_history'] = 'false'
        with patch('opentf.tools.ctlplugin_gitlab._post_note', mock_pmn), patch(
            'opentf.tools.ctlplugin_gitlab._maybe_get_noteid', mock_ahn
        ), patch('requests.delete', mock_delete), patch(
            'opentf.tools.ctlplugin_gitlab._warning', mock_warning
        ):
            publish_results(params, 'mode', QG_RESPONSE_SUCCESS)
        mock_warning.assert_called_once()
        self.assertIn(
            'Failed to remove previous quality gate results',
            mock_warning.call_args[0][0],
        )
        self.assertEqual(401, mock_warning.call_args[0][1])

    def test_update_results_api_exception(self):
        mock_pmn = MagicMock(return_value=({'id': '123'}, 201))
        mock_request = MagicMock(side_effect=HTTPError('Boom'))
        mock_error = MagicMock()
        params = GL_PARAMS.copy()
        params['keep_history'] = 'false'
        with patch('opentf.tools.ctlplugin_gitlab._post_note', mock_pmn), patch(
            'requests.get', mock_request
        ), patch('opentf.tools.ctlplugin_gitlab._error', mock_error):
            publish_results(params, 'mode', QG_RESPONSE_SUCCESS)
        mock_pmn.assert_called_once()
        mock_request.assert_called_once()
        self.assertEqual(
            params['token'], mock_request.call_args[1]['params']['private_token']
        )
        self.assertIn(
            'Exception while retrieving notes from Gitlab', mock_error.call_args[0][0]
        )
        self.assertIn('Boom', mock_error.call_args[0][1])

    def test_if_has_note_yes(self):
        mockresponse = _make_response(200, [{'id': 346, 'body': 'one mode fits all'}])
        mock_get = MagicMock(return_value=mockresponse)
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlplugin_gitlab.NOTE_HEADER_TEMPLATE', 'one {mode} fits all'
        ):
            note_id = _maybe_get_noteid('url/go/home', 'mode', 345, None)
        self.assertEqual(346, note_id)

    def test_if_has_note_no(self):
        mockresponse = _make_response(200, [{'id': 346, 'body': 'one mode fits all'}])
        mock_get = MagicMock(return_value=mockresponse)
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlplugin_gitlab.NOTE_HEADER_TEMPLATE', 'one {mode} fits all'
        ):
            note_id = _maybe_get_noteid('url/go/home', 'mode', 346, None)
        self.assertIsNone(note_id)

    def test_if_has_note_bad_api_response(self):
        mockresponse = _make_response(404, {'error': 'Not found'})
        mock_get = MagicMock(return_value=mockresponse)
        mock_warning = MagicMock()
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlplugin_gitlab.NOTE_HEADER_TEMPLATE', 'one {mode} fits all'
        ), patch('opentf.tools.ctlplugin_gitlab._warning', mock_warning):
            _maybe_get_noteid('url/go/home', 'mode', 346, None)
        mock_warning.assert_called_once()
        self.assertIn('Cannot retrieve notes from Gitlab', mock_warning.call_args[0][0])
        self.assertEqual(404, mock_warning.call_args[0][1])

    def test_format_response_status_failure_with_warnings(self):
        formatted = _format_qualitygate_response('mode', QG_RESPONSE_FAILURE, False)
        self.assertIn('Quality gate failed', formatted)
        self.assertIn('Keep your eyes open', formatted)

    def test_format_response_status_success(self):
        formatted = _format_qualitygate_response('mode', QG_RESPONSE_SUCCESS, False)
        self.assertIn('Quality gate passed', formatted)
        self.assertIn('RULE A', formatted)

    def test_format_response_status_notest(self):
        formatted = _format_qualitygate_response('mode', QG_RESPONSE_NOTEST, False)
        self.assertIn('Quality gate not applied', formatted)
        self.assertIn('no test', formatted)

    def test_format_response_with_cats(self):
        formatted = _format_qualitygate_response('mode', QG_RESPONSE_NOTEST, True)
        self.assertIn('Quality gate not applied', formatted)
        self.assertIn('_cat', formatted)

    def test_dispatch_results_for_mr(self):
        mock_publish = MagicMock()
        with patch('opentf.tools.ctlplugin_gitlab.publish_results', mock_publish):
            dispatch_results_by_target_type({'mr': '2'}, 'mode', {'a': 'b'})
        mock_publish.assert_called_once_with(
            {'mr': '2', 'target': 'merge_requests', 'target_id': '2'},
            'mode',
            {'a': 'b'},
        )

    def test_dispatch_results_for_issue(self):
        mock_publish = MagicMock()
        with patch('opentf.tools.ctlplugin_gitlab.publish_results', mock_publish):
            dispatch_results_by_target_type({'issue': '32'}, 'mode', {'a': 'b'})
        mock_publish.assert_called_once_with(
            {'issue': '32', 'target': 'issues', 'target_id': '32'}, 'mode', {'a': 'b'}
        )

    def test_dispatch_results_for_mr_and_issue(self):
        called_as = []

        def mock_publish(a, b, c):
            called_as.append(a.copy())

        with patch('opentf.tools.ctlplugin_gitlab.publish_results', mock_publish):
            dispatch_results_by_target_type(
                {'issue': '444', 'mr': '332'}, 'mode', {'a': 'b'}
            )
        self.assertEqual(2, len(called_as))
        self.assertEqual('merge_requests', called_as[0]['target'])
        self.assertEqual('issues', called_as[1]['target'])

    def test_dispatch_results_no_test_no_issue(self):
        mock_error = MagicMock()
        with patch('opentf.tools.ctlplugin_gitlab._error', mock_error):
            dispatch_results_by_target_type(
                {'project': 'bar', 'keep_history': 'true'},
                'mode',
                {'a': 'b'},
            )
        calls = mock_error.call_args[0]
        self.assertEqual(
            'Cannot post results to GitLab, missing mandatory parameters: %s.', calls[0]
        )
        self.assertEqual('server, target, target-id', calls[1])

    def test_label_target_ok_no_previous_labels(self):
        mock_get_response = _make_response(200, {'no': 'labels'})
        mock_put_response = _make_response(200, {'labels': 'added'})
        mock_get = MagicMock(return_value=mock_get_response)
        mock_put = MagicMock(return_value=mock_put_response)
        mock_warning = MagicMock()

        with patch('requests.get', mock_get), patch('requests.put', mock_put), patch(
            'opentf.tools.ctlplugin_gitlab._warning', mock_warning
        ):
            set_label('url', 'PreFix', 'SUCCESS', {'token': 'a'})
        mock_get.assert_called_once_with('url', params={'token': 'a'}, timeout=10)
        mock_put.assert_called_once_with(
            'url', params={'add_labels': 'PreFix::Passed', 'token': 'a'}, timeout=10
        )
        mock_warning.assert_not_called()

    def test_label_target_previous_label_updated(self):
        mock_get_response = _make_response(200, {'labels': ['PostFix::Failed']})
        mock_remove_response = _make_response(200, {'labels': 'removed'})
        mock_add_response = _make_response(200, {'labels': 'added'})
        mock_get = MagicMock(return_value=mock_get_response)
        mock_put = MagicMock(side_effect=[mock_remove_response, mock_add_response])
        mock_warning = MagicMock()

        with patch('requests.get', mock_get), patch('requests.put', mock_put), patch(
            'opentf.tools.ctlplugin_gitlab._warning', mock_warning
        ):
            set_label('url', 'PostFix', 'NOTEST', {'a': 'b'})
        mock_get.assert_called_once_with('url', params={'a': 'b'}, timeout=10)
        self.assertEqual(2, mock_put.call_count)
        msgs = mock_put.call_args_list
        self.assertEqual(
            {'remove_labels': 'PostFix::Failed', 'a': 'b'}, msgs[0][1]['params']
        )
        self.assertEqual(
            {'add_labels': 'PostFix::No test', 'a': 'b'}, msgs[1][1]['params']
        )
        mock_warning.assert_not_called()

    def test_label_previous_label_same_as_current(self):
        mock_get_response = _make_response(200, {'labels': ['HotFix::Passed']})
        mock_get = MagicMock(return_value=mock_get_response)
        mock_put = MagicMock()
        mock_warning = MagicMock()

        with patch('requests.get', mock_get), patch('requests.put', mock_put), patch(
            'opentf.tools.ctlplugin_gitlab._warning', mock_warning
        ):
            set_label('url', 'HotFix', 'SUCCESS', None)
        mock_get.assert_called_once_with('url', params=None, timeout=10)
        mock_put.assert_not_called()
        mock_warning.assert_not_called()

    def test_label_target_failed_to_get_old(self):
        mock_get_response = _make_response(404, {'error': 'error'})
        mock_put_response = _make_response(200, {'ok': 'ok'})
        mock_get = MagicMock(return_value=mock_get_response)
        mock_put = MagicMock(return_value=mock_put_response)
        mock_warning = MagicMock()

        with patch('requests.get', mock_get), patch('requests.put', mock_put), patch(
            'opentf.tools.ctlplugin_gitlab._warning', mock_warning
        ):
            set_label('url', 'UnFix', 'SUCCESS', {'token': 'a'})
        mock_get.assert_called_once_with('url', params={'token': 'a'}, timeout=10)
        mock_put.assert_called_once_with(
            'url', params={'add_labels': 'UnFix::Passed', 'token': 'a'}, timeout=10
        )
        mock_warning.assert_called_once_with(
            'Cannot retrieve labels from GitLab target: status code %d, error %s.',
            404,
            {'error': 'error'},
        )

    def test_label_target_failed_to_remove_old(self):
        mock_get_response = _make_response(200, {'labels': ['ReFix::Failed']})
        mock_remove_response = _make_response(500, {'error': 'serverside'})
        mock_add_response = _make_response(200, {'labels': 'added'})
        mock_get = MagicMock(return_value=mock_get_response)
        mock_put = MagicMock(side_effect=[mock_remove_response, mock_add_response])
        mock_warning = MagicMock()

        with patch('requests.get', mock_get), patch('requests.put', mock_put), patch(
            'opentf.tools.ctlplugin_gitlab._warning', mock_warning
        ):
            set_label('url', 'ReFix', 'NOTEST', {'a': 'b'})
        mock_get.assert_called_once_with('url', params={'a': 'b'}, timeout=10)
        self.assertEqual(2, mock_put.call_count)
        mock_warning.assert_called_once_with(
            'Cannot remove labels `%s` from GitLab target: status code %d, error %s.',
            'ReFix::Failed',
            500,
            {'error': 'serverside'},
        )

    def test_label_target_failed_to_add_new(self):
        mock_get_response = _make_response(200, {'no': 'labels'})
        mock_put_response = _make_response(999, {'error': 'error'})
        mock_get = MagicMock(return_value=mock_get_response)
        mock_put = MagicMock(return_value=mock_put_response)
        mock_warning = MagicMock()

        with patch('requests.get', mock_get), patch('requests.put', mock_put), patch(
            'opentf.tools.ctlplugin_gitlab._warning', mock_warning
        ):
            set_label('url', 'PreFix', 'SUCCESS', {'token': 'a'})
        mock_get.assert_called_once_with('url', params={'token': 'a'}, timeout=10)
        mock_put.assert_called_once_with(
            'url', params={'add_labels': 'PreFix::Passed', 'token': 'a'}, timeout=10
        )
        mock_warning.assert_called_once_with(
            'Failed to label GitLab target with `%s`: status code %d, error %s.',
            'PreFix::Passed',
            999,
            {'error': 'error'},
        )

    def test_label_target_ko(self):
        mock_get = MagicMock(side_effect=Exception('No labeling today'))
        mock_put = MagicMock()
        mock_error = MagicMock()

        with patch('requests.get', mock_get), patch('requests.put', mock_put), patch(
            'opentf.tools.ctlplugin_gitlab._error', mock_error
        ):
            set_label('url', 'PreFix', 'SUCCESS', {'token': 'a'})
        mock_error.assert_called_once_with(
            'Error while labeling GitLab target: %s.', 'No labeling today'
        )
        mock_put.assert_not_called()
