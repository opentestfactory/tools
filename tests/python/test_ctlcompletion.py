# Copyright (c) 2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import unittest
import sys

from io import StringIO
from unittest.mock import MagicMock, patch

from opentf.tools.ctlcompletion import COMMANDS, completion_cmd, get_suggestions, main

########################################################################


class TestCtlAttachments(unittest.TestCase):
    # def setUp(self):
    #     self.held_output = StringIO()
    #     sys.stdout = self.held_output
    #     sys.stderr = self.held_output

    # def tearDown(self):
    #     self.held_output.close()
    #     sys.stdout = sys.__stdout__
    #     sys.stderr = sys.__stderr__

    # get_suggestions

    def test_get_suggestions_0(self):
        args = []
        sugg = get_suggestions(args, '')
        self.assertEqual(list(COMMANDS), sugg)

    def test_get_suggestions_0_1(self):
        args = ['de']
        sugg = get_suggestions(args, 'de')
        self.assertEqual(['delete', 'describe'], sugg)

    def test_get_suggestions_1(self):
        args = ['generate']
        sugg = get_suggestions(args, '')
        self.assertEqual(['token', 'report'], sugg)

    def test_get_suggestions_1_1(self):
        args = ['generate', 'to']
        sugg = get_suggestions(args, 'to')
        self.assertEqual(['token'], sugg)

    def test_get_suggestions_wf_id(self):
        args = ['get', 'workflow']
        getters = {'workflow_id': MagicMock(return_value=['abc', 'def'])}
        with patch('opentf.tools.ctlcompletion.PARAMS_GETTERS', getters), patch(
            'opentf.tools.ctlcompletion.read_configuration'
        ):
            sugg = get_suggestions(args, '')
        self.assertEqual(['abc', 'def'], sugg)

    def test_get_suggestions_agent_id_1(self):
        args = ['delete', 'agent', 'e2']
        agents = [{'metadata': {'agent_id': 'e23'}}, {'metadata': {'agent_id': 'd34'}}]
        mock_get = MagicMock(return_value=agents)
        with patch('opentf.tools.ctl._get_agents', mock_get), patch(
            'opentf.tools.ctlcompletion.read_configuration'
        ):
            sugg = get_suggestions(args, 'e2')
        self.assertEqual(['e23'], sugg)

    def test_get_suggestions_sub_id_1_2(self):
        args = ['delete', 'subscription', '12']
        mock_get = MagicMock(return_value={'1234': {}, '5678': {}, '1200': {}})
        with patch('opentf.tools.ctl._get_subscriptions', mock_get), patch(
            'opentf.tools.ctlcompletion.read_configuration'
        ):
            sugg = get_suggestions(args, '12')
        self.assertEqual(['1234', '1200'], sugg)

    def test_get_suggestions_filepath(self):
        args = ['run', 'workflow']
        sugg = get_suggestions(args, '')
        self.assertEqual(['__file__'], sugg)

    def test_get_suggestions_bad_first_param(self):
        args = ['foo']
        sugg = get_suggestions(args, '')
        self.assertEqual([], sugg)

    def test_get_suggestions_config(self):
        args = ['config', 'use-context', 'f']
        config = ('', {'contexts': [{'name': 'foo'}, {'name': 'bar'}]})
        with patch(
            'opentf.tools.ctlcompletion._read_opentfconfig', return_value=config
        ), patch('opentf.tools.ctlcompletion.read_configuration'):
            sugg = get_suggestions(args, 'f')
        self.assertEqual(['foo'], sugg)

    def test_get_suggestions_generate_report_0(self):
        args = ['generate', 'report', 'a']
        getters = {'workflow_id': MagicMock(return_value=['abc', 'def'])}
        with patch('opentf.tools.ctlcompletion.PARAMS_GETTERS', getters), patch(
            'opentf.tools.ctlcompletion.read_configuration'
        ):
            sugg = get_suggestions(args, 'a')
        self.assertEqual(['abc'], sugg)

    def test_get_suggestions_generate_report_1(self):
        args = ['generate', 'report', '1r2e3p']
        sugg = get_suggestions(args, '')
        self.assertEqual(['using'], sugg)

    def test_get_suggestions_generate_report_1_1(self):
        args = ['generate', 'report', '1r2e3p', 'us']
        sugg = get_suggestions(args, 'us')
        self.assertEqual(['using'], sugg)

    def test_get_suggestions_generate_report_2(self):
        args = ['generate', 'report', '1r2e3p', 'using', 'pat']
        sugg = get_suggestions(args, 'pat')
        self.assertEqual(['__file__'], sugg)

    def test_get_suggestions_generate_report_2_1_options(self):
        args = ['generate', 'report', '1r2e3p', 'using', 'path']
        sugg = get_suggestions(args, '')
        self.assertEqual(['--timeout', '--name', '--save-to', '--as'], sugg)

    def test_get_suggestions_options_1(self):
        args = ['delete', 'agent', '1234']
        sugg = get_suggestions(args, '')
        self.assertEqual(['--selector', '--field-selector', '--all'], sugg)

    def test_get_suggestions_options_1_1(self):
        args = ['delete', 'agent', '1234', '--s']
        sugg = get_suggestions(args, '--s')
        self.assertEqual(['--selector'], sugg)

    def test_get_suggestions_options_2(self):
        args = ['get', 'agents']
        sugg = get_suggestions(args, '')
        self.assertEqual(['--output', '--selector', '--field-selector'], sugg)

    def test_get_suggestions_options_2_2(self):
        args = ['get', 'agents', '--o']
        sugg = get_suggestions(args, '--o')
        self.assertEqual(['--output'], sugg)

    def test_get_suggestions_options_args_1(self):
        args = ['get', 'workflow', 'ced', '--output']
        sugg = get_suggestions(args, '')
        self.assertEqual(['wide', 'custom-columns', 'json', 'yaml'], sugg)

    def test_get_suggestions_options_args_1_1(self):
        args = ['get', 'workflow', 'ced', '--output=']
        sugg = get_suggestions(args, '--output=')
        self.assertEqual(['wide', 'custom-columns', 'json', 'yaml'], sugg)

    def test_get_suggestions_options_args_1_2(self):
        args = ['get', 'workflow', 'ced', '--output=y']
        sugg = get_suggestions(args, '--output=y')
        self.assertEqual(['yaml'], sugg)

    def test_get_suggestions_options_args_2(self):
        args = ['get', 'workflow', 'ced', '--output', 'y']
        sugg = get_suggestions(args, 'y')
        self.assertEqual(['yaml'], sugg)

    def test_get_suggestions_options_args_3(self):
        args = ['get', 'qualitygate', 'ced', '--using']
        sugg = get_suggestions(args, '')
        self.assertEqual(['__file__'], sugg)

    def test_get_suggestions_options_args_3_1(self):
        args = ['get', 'qualitygate', 'ced', '--using', 'pat']
        sugg = get_suggestions(args, 'pat')
        self.assertEqual(['__file__'], sugg)

    def test_get_suggestions_options_args_double_1(self):
        args = ['get', 'qualitygate', 'def', '--plugin']
        sugg = get_suggestions(args, '')
        self.assertEqual(['gitlab:'], list(sugg))

    def test_get_suggestions_options_args_double_1_1(self):
        args = ['get', 'qualitygate', 'def', '--plugin', 'gitlab:']
        sugg = get_suggestions(args, 'gitlab:')
        self.assertEqual(
            ['server', 'project', 'mr', 'issue', 'keep-history', 'token', 'label'], sugg
        )

    def test_get_suggestions_options_args_double_1_2(self):
        args = ['get', 'qualitygate', 'def', '--plugin', 'gitlab:kee']
        sugg = get_suggestions(args, 'gitlab:kee')
        self.assertEqual(['keep-history'], sugg)

    def test_get_suggestions_options_args_double_2_1(self):
        args = ['get', 'qualitygate', 'def', '--plugin=gitlab:']
        sugg = get_suggestions(args, '--plugin=gitlab:')
        self.assertEqual(
            ['server', 'project', 'mr', 'issue', 'keep-history', 'token', 'label'], sugg
        )

    def test_get_suggestions_options_args_double_2_2(self):
        args = ['get', 'qualitygate', 'def', '--plugin=gitlab:tok']
        sugg = get_suggestions(args, '--plugin=gitlab:tok')
        self.assertEqual(['token'], sugg)

    def test_get_suggestions_options_args_double_2_3(self):
        args = ['get', 'qualitygate', 'def', '--plugin=gitlab:token']
        sugg = get_suggestions(args, '')
        self.assertEqual(
            ['--output', '--mode', '--using', '--plugin', '--timeout'], sugg
        )

    def test_get_suggestions_double_param_1(self):
        args = ['cp']
        wfs = ['123', '456']
        with patch(
            'opentf.tools.ctlcompletion._get_workflows', return_value=wfs
        ) as mock_wf, patch('opentf.tools.ctlcompletion.read_configuration'):
            sugg = get_suggestions(args, '')
        mock_wf.assert_called_once()
        self.assertEqual(wfs, sugg)

    def test_get_suggestions_double_param_1_1(self):
        args = ['cp', '1']
        wfs = ['123', '456', '789']
        with patch(
            'opentf.tools.ctlcompletion._get_workflows', return_value=wfs
        ) as mock_wf, patch('opentf.tools.ctlcompletion.read_configuration'):
            sugg = get_suggestions(args, '1')
        mock_wf.assert_called_once()
        self.assertEqual(['123:'], sugg)

    def test_get_suggestions_double_param_2(self):
        args = ['cp', '123:']
        wfs = ['123', '456', '789']
        atts = ['a2a', 'b2b', 'c2c']
        with patch(
            'opentf.tools.ctlcompletion._get_workflows', return_value=wfs
        ) as mock_wf, patch(
            'opentf.tools.ctlcompletion._get_attachment_uuids', return_value=atts
        ) as mock_att, patch(
            'opentf.tools.ctlcompletion.read_configuration'
        ):
            sugg = get_suggestions(args, '123:')
        mock_wf.assert_called_once()
        mock_att.assert_called_once()
        self.assertEqual(atts, sugg)

    def test_get_suggestions_double_param_2_1(self):
        args = ['cp', '123:a']
        wfs = ['123', '456', '789']
        atts = ['a2a', 'b2b', 'c2c']
        with patch(
            'opentf.tools.ctlcompletion._get_workflows', return_value=wfs
        ) as mock_wf, patch(
            'opentf.tools.ctlcompletion._get_attachment_uuids', return_value=atts
        ) as mock_att, patch(
            'opentf.tools.ctlcompletion.read_configuration'
        ):
            sugg = get_suggestions(args, '123:a')
        mock_wf.assert_called_once()
        mock_att.assert_called_once()
        self.assertEqual(['a2a'], sugg)

    def test_get_suggestions_options_services_1(self):
        args = ['config', 'set-orchestrator', 'local', '--ag']
        sugg = get_suggestions(args, '--ag')
        self.assertEqual(
            [
                '--agentchannel-force-base-url',
                '--agentchannel-port',
                '--agentchannel-prefix',
            ],
            sugg,
        )

    def test_get_suggestions_options_services_1_1(self):
        args = ['config', 'set-orchestrator', 'local', '--observer-force-base-url=']
        sugg = get_suggestions(args, '--observer-force-base-url=')
        self.assertEqual(
            ['true', 'false'],
            sugg,
        )

    def test_get_suggestions_options_version_1(self):
        args = ['version']
        sugg = get_suggestions(args, '')
        self.assertEqual(['--debug'], sugg)

    def test_get_suggestions_options_version_1_1(self):
        args = ['version', '--de']
        sugg = get_suggestions(args, '--de')
        self.assertEqual(['--debug'], sugg)

    def test_get_suggestions_options_cp_1(self):
        args = ['cp', 'abc:def', 'fpath']
        sugg = get_suggestions(args, '')
        self.assertEqual(['--type'], sugg)

    def test_get_suggestions_options_cp_1_1(self):
        args = ['cp', 'def:ghi', '--t']
        sugg = get_suggestions(args, '--t')
        self.assertEqual(['--type'], sugg)

    def test_get_suggestions_options_ef_1(self):
        args = ['run', 'workflow', 'wf.yaml', '-e']
        sugg = get_suggestions(args, '')
        self.assertEqual('--output', sugg[0])

    def test_get_suggestions_options_ef_1_1(self):
        args = ['run', 'workflow', 'wf.yaml', '-f', 'fi']
        sugg = get_suggestions(args, 'fi')
        self.assertEqual(['__file__'], sugg)

    # main

    def test_main(self):
        args = ['opentf-ctl', 'get', 'att', 'att']
        with patch('builtins.print') as mock_print, patch('sys.argv', args):
            main()
        mock_print.assert_called_once_with('attachments')

    # # completion_cmd

    def test_completion_cmd_ok(self):
        with patch(
            'opentf.tools.ctlcompletion._is_command', return_value=True
        ) as mock_cmd, patch('builtins.print') as mock_print:
            completion_cmd()
        mock_cmd.assert_called_once()
        mock_print.assert_called_once()
        self.assertNotIn('{ctlcompletion_path}', mock_print.call_args[0][0])

    def test_completion_cmd_ko(self):
        with patch(
            'opentf.tools.ctlcompletion._is_command', return_value=False
        ) as mock_cmd, patch('opentf.tools.ctlcompletion._error') as mock_error:
            self.assertRaisesRegex(SystemExit, '1', completion_cmd)
        mock_cmd.assert_called_once()
        mock_error.assert_called_once()
        self.assertIn('Unknown command', mock_error.call_args[0][0])
