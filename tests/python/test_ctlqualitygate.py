# Copyright (c) 2022-2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""get qualitygate unit tests."""

import logging
import unittest
import sys

from io import StringIO
from unittest.mock import MagicMock, patch, mock_open

from requests import Response

from opentf.tools.ctlqualitygate import (
    _post_qualitygate_definition,
    _query_qualitygate,
    get_qualitygate,
    qualitygate_cmd,
    print_qualitygate_help,
    _add_gl_params,
)


########################################################################
# Datasets

CONFIG = {
    'token': 'AAAABBBBCCCCDDDDEEEEFFFF0000111122223333',
    'orchestrator': {
        'insecure-skip-tls-verify': True,
        'services': {
            'eventbus': {'port': 1234},
            'receptionist': {'port': 4321, 'prefix': 'reCeptioNist'},
            'observer': {'port': 9999, 'force-base-url': True},
            'killswitch': {'port': 8888},
        },
        'server': 'http://aaaa',
    },
}

CONFIG_LEGACY = {
    'token': 'AAAABBBBCCCCDDDDEEEEFFFF0000111122223333',
    'orchestrator': {
        'insecure-skip-tls-verify': True,
        'ports': {
            'eventbus': 1234,
            'receptionist': 4321,
            'observer': 9999,
            'killswitch': 8888,
        },
        'server': 'http://aaaa',
    },
}

HEADERS = {'Authorization': 'Bearer ' + CONFIG_LEGACY['token']}

UUID_OK = '6120ee73-2296-4dc6-9b3b-b6f2d4b4a622'
UUID_BAD = 'z6120ee73-2296-4dc6-9b3b-b6f2d4b4a622'

########################################################################
# Helpers

mockresponse = Response()
mockresponse.status_code = 200


def _make_response(status_code, json_value):
    mock = Response()
    mock.status_code = status_code
    mock.json = lambda: json_value  # pyright: ignore
    return mock


class TestCtlQualitygate(unittest.TestCase):
    """Unit tests for ctlqualitygate."""

    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    def setUp(self):
        self.held_output = StringIO()
        sys.stdout = self.held_output
        sys.stderr = self.held_output

    def tearDown(self):
        self.held_output.close()
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__

    # get_qualitygate

    def test_get_qualitygate_tech_status_notest(self):
        mock = MagicMock(return_value={'details': {'status': 'NOTEST'}})
        mock_print = MagicMock()
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'builtins.print', mock_print
        ):
            get_qualitygate(UUID_OK, 'mOdE')
        self.assertTrue('mode=mOdE' in mock.call_args_list[0][0][1])
        mock_print.assert_called_once_with(
            'Workflow 6120ee73-2296-4dc6-9b3b-b6f2d4b4a622 contains no test matching quality gate scopes.'
        )

    def test_get_qualitygate_tech_status_unexpected(self):
        mock = MagicMock(return_value={'details': {'status': 'UNEXPECTED'}})
        mock_exit = MagicMock()
        mock_exit.side_effect = SystemExit()
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'sys.exit', mock_exit
        ):
            self.assertRaises(SystemExit, get_qualitygate, UUID_OK, 'mOdE')
        self.assertTrue('mode=mOdE' in mock.call_args_list[0][0][1])
        self.assertEqual(mock_exit.call_args_list[0][0][0], 2)

    def test_get_qualitygate_tech_status_empty(self):
        mock = MagicMock(return_value={})
        mock_exit = MagicMock()
        mock_exit.side_effect = SystemExit()
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'sys.exit', mock_exit
        ):
            self.assertRaises(SystemExit, get_qualitygate, UUID_OK, 'mOdE')
        self.assertTrue('mode=mOdE' in mock.call_args_list[0][0][1])
        mock_exit.assert_called_once()
        self.assertEqual(mock_exit.call_args_list[0][0][0], 2)

    def test_get_qualitygate_tech_status_running(self):
        mock = MagicMock(return_value={'details': {'status': 'RUNNING'}})
        mock_exit = MagicMock()
        mock_exit.side_effect = SystemExit()
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'sys.exit', mock_exit
        ):
            self.assertRaises(SystemExit, get_qualitygate, UUID_OK, 'mOdE')
        self.assertTrue('mode=mOdE' in mock.call_args_list[0][0][1])
        self.assertEqual(mock_exit.call_args_list[0][0][0], 101)

    def test_get_qualitygate_tech_status_failure(self):
        mock = MagicMock(return_value={'details': {'status': 'FAILURE'}})
        mock_exit = MagicMock()
        mock_exit.side_effect = SystemExit()
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'sys.exit', mock_exit
        ):
            self.assertRaises(SystemExit, get_qualitygate, UUID_OK, 'mOdE')
        self.assertTrue('mode=mOdE' in mock.call_args_list[0][0][1])
        self.assertEqual(mock_exit.call_args_list[0][0][0], 102)

    def test_get_qualitygate_exec_status_success(self):
        qg = {
            'mbr': {
                'result': 'SUCCESS',
                'success_ratio': '100.0%',
                'tests_failed': 0,
                'tests_in_scope': 10,
                'tests_passed': 10,
            }
        }
        rule = qg['mbr']
        mock_et = MagicMock()
        mock = MagicMock(return_value={'details': {'rules': qg, 'status': 'SUCCESS'}})
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlcommons._emit_table', mock_et
        ):
            get_qualitygate(UUID_OK, 'mYGate')
        self.assertTrue('mode=mYGate' in mock.call_args_list[0][0][1])
        mock_et.assert_called_once()
        data = list(mock_et.call_args[0][0])
        self.assertEqual(len(data[0]), len(mock_et.call_args[0][1]))
        self.assertEqual(
            [
                'mbr',
                rule['result'],
                str(rule['tests_in_scope']),
                str(rule['tests_failed']),
                str(rule['tests_passed']),
                rule['success_ratio'],
            ],
            data[0],
        )

    def test_get_qualitygate_exec_status_failure_wide_columns(self):
        qg = {
            'not mbr': {
                'result': 'FAILURE',
                'scope': 'contains all',
                'success_ratio': '55.0%',
                'tests_failed': 44,
                'tests_in_scope': 45,
                'tests_passed': 1,
                'threshold': '56%',
            }
        }
        mock_go = MagicMock()
        mock = MagicMock(return_value={'details': {'rules': qg, 'status': 'FAILURE'}})
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlqualitygate.generate_output', mock_go
        ), patch(
            'sys.argv', ['dummy', '--output', 'wide']
        ):
            self.assertRaises(SystemExit, get_qualitygate, UUID_OK, 'mYGate')
        self.assertTrue('mode=mYGate' in mock.call_args_list[0][0][1])
        mock_go.assert_called_once()
        data = mock_go.call_args[0][0]
        self.assertEqual(len(data), 1)
        self.assertIn('rule', data[0])
        self.assertIn('name', data[0]['rule'])
        self.assertEqual(data[0]['rule']['name'], 'not mbr')

    def test_get_qualitygate_exec_status_notest(self):
        qg = {
            'not at all mbr': {
                'result': 'NOTEST',
            }
        }
        mock_et = MagicMock()
        mock = MagicMock(return_value={'details': {'rules': qg, 'status': 'NOTEST'}})
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlcommons._emit_table', mock_et
        ):
            get_qualitygate(UUID_OK, 'mYGate')
        self.assertTrue('mode=mYGate' in mock.call_args_list[0][0][1])
        mock_et.assert_called_once()
        data = list(mock_et.call_args[0][0])
        self.assertEqual(len(data[0]), len(mock_et.call_args[0][1]))
        self.assertEqual(
            [
                'not at all mbr',
                qg['not at all mbr']['result'],
                '<none>',
                '<none>',
                '<none>',
                '<none>',
            ],
            data[0],
        )

    def test_get_qualitygate_exec_status_empty(self):
        qg = {}
        mock_et = MagicMock()
        mock = MagicMock(return_value={'details': {'rules': qg, 'status': 'SUCCESS'}})
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlcommons._emit_table', mock_et
        ):
            get_qualitygate(UUID_OK, 'mYGate')
        self.assertTrue('mode=mYGate' in mock.call_args_list[0][0][1])
        mock_et.assert_called_once()
        data = list(mock_et.call_args[0][0])
        self.assertFalse(data)

    def test_get_qualitygate_exec_status_unexpected(self):
        qg = {
            'what rules': {
                'result': 'NO STATUS',
            }
        }
        mock_et = MagicMock()
        mock = MagicMock(
            return_value={'details': {'rules': qg, 'status': 'UNEXPECTED'}}
        )
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlcommons._emit_table', mock_et
        ):
            self.assertRaises(SystemExit, get_qualitygate, UUID_OK, 'mYGate')
        self.assertTrue('mode=mYGate' in mock.call_args_list[0][0][1])
        mock_et.assert_not_called()

    def test_get_qualitygate_with_warnings(self):
        qg = {
            'somERule': {
                'result': 'NOTEST',
            }
        }
        mock_et = MagicMock()
        mock_warning = MagicMock()
        mock = MagicMock(
            return_value={
                'details': {
                    'rules': qg,
                    'status': 'NOTEST',
                    'warnings': ['Winter is coming'],
                }
            }
        )
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlcommons._emit_table', mock_et
        ), patch(
            'opentf.tools.ctlqualitygate._warning', mock_warning
        ):
            get_qualitygate(UUID_OK, 'mYGate')
            mock_warning.assert_called_once_with('Winter is coming')

    def test_get_qualitygate_with_scope_errors(self):
        qg = {'somERule': {'result': 'INVALID_SCOPE', 'message': 'Invalid scope.'}}
        mock_et = MagicMock()
        mock_error = MagicMock()
        mock = MagicMock(
            return_value={
                'details': {
                    'rules': qg,
                    'status': 'NOTEST',
                }
            }
        )
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlcommons._emit_table', mock_et
        ), patch(
            'opentf.tools.ctlqualitygate._error', mock_error
        ):
            get_qualitygate(UUID_OK, 'mYGate')
            mock_error.assert_called_once_with(
                'Error in rule `somERule`. Invalid scope.'
            )

    def test_get_qualitygate_timeout_ko(self):
        with patch(
            'opentf.tools.ctlqualitygate._query_qualitygate',
            MagicMock(return_value={'status': 'FOO'}),
        ) as mock_query, patch('opentf.tools.ctlqualitygate._warning') as mock_warning:
            self.assertRaisesRegex(
                SystemExit, '2', get_qualitygate, UUID_OK, 'mYGate', timeout='-2'
            )
        mock_query.assert_called_once_with(UUID_OK, None, 'mYGate', 8)
        mock_warning.assert_called_once_with(
            'Timeout must be a positive integer, got %s, resetting to default value.',
            '-2',
        )

    # post qualitygate definition

    def test_get_qualitygate_with_using_option(self):
        mock_et = MagicMock()
        mock_gqd = MagicMock(return_value={'status': 'FAILURE'})
        with patch('opentf.tools.ctlqualitygate._query_qualitygate', mock_gqd), patch(
            'opentf.tools.ctlcommons._emit_table', mock_et
        ):
            self.assertRaises(
                SystemExit, get_qualitygate, UUID_OK, 'mYGate', False, '/path/to/file'
            )
        mock_gqd.assert_called_once_with(UUID_OK, '/path/to/file', 'mYGate', 8)

    def test_get_qualitygate_with_using_option_and_warnings(self):
        mock_et = MagicMock()
        mock_gqd = MagicMock(
            return_value={'status': 'FAILURE', 'warnings': ['The time is out of joint']}
        )
        mock_warning = MagicMock()
        with patch('opentf.tools.ctlqualitygate._query_qualitygate', mock_gqd), patch(
            'opentf.tools.ctlcommons._emit_table', mock_et
        ), patch('opentf.tools.ctlqualitygate._warning', mock_warning):
            self.assertRaises(
                SystemExit, get_qualitygate, UUID_OK, 'mYGate', False, '/path/to/file'
            )
        mock_warning.assert_called_once_with('The time is out of joint')

    def test_query_qualitygate_ok(self):
        mockresponse = _make_response(200, {'details': {'status': 'NOTEST'}})
        mock = MagicMock(return_value=mockresponse)
        mock_read = mock_open(read_data='')
        mock_post = MagicMock(
            return_value={'details': {'status': 'NOTEST', 'rules': {}}, 'code': 200}
        )
        with patch('builtins.open', mock_read), patch(
            'opentf.tools.ctlqualitygate._post', mock_post
        ), patch('requests.post', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            result = _query_qualitygate(UUID_OK, 'fiLepAth', 'nOtmYgAtE', 30)
            self.assertEqual({'status': 'NOTEST', 'rules': {}}, result)
            self.assertIn('mode=nOtmYgAtE', mock_post.call_args[0][1])

    def test_query_qualitygate_returns_202(self):
        mockresponse = _make_response(202, {})
        mock = MagicMock(return_value=mockresponse)
        mock_read = mock_open(read_data='')
        mock_post = MagicMock(return_value={'code': 202})
        with patch('builtins.open', mock_read), patch(
            'builtins.print'
        ) as mock_print, patch('opentf.tools.ctlqualitygate._post', mock_post), patch(
            'requests.post', mock
        ), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ), self.assertRaises(
            SystemExit
        ) as ex:
            _query_qualitygate(UUID_OK, 'fiLepAth', 'nOtmYgAtE', 30)
        self.assertEqual('101', str(ex.exception))
        mock_print.assert_called_once_with(
            f'Workflow {UUID_OK} events caching still in progress. Please retry later.'
        )

    def test_query_qualitygate_returns_404(self):
        mockresponse = _make_response(404, {})
        mock = MagicMock(return_value=mockresponse)
        mock_read = mock_open(read_data='')
        mock_post = MagicMock(return_value={'code': 404})
        mock_fatal = MagicMock(side_effect=SystemExit(2))
        with patch('builtins.open', mock_read), patch(
            'opentf.tools.ctlqualitygate._post', mock_post
        ), patch('requests.post', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ), patch(
            'opentf.tools.ctlqualitygate._fatal', mock_fatal
        ), self.assertRaises(
            SystemExit
        ) as ex:
            _query_qualitygate(UUID_OK, 'fiLepAth', 'nOtmYgAtE', 30)
        mock_fatal.assert_called_once()
        self.assertIn('Unknown workflow', mock_fatal.call_args[0][0])
        self.assertEqual('2', str(ex.exception))

    def test_query_qualitygate_returns_422(self):
        mockresponse = _make_response(422, {'message': 'Error 422.'})
        mock = MagicMock(return_value=mockresponse)
        mock_read = mock_open(read_data='')
        mock_post = MagicMock(return_value={'code': 422, 'message': 'Error 422.'})
        mock_fatal = MagicMock(side_effect=SystemExit(2))
        with patch('builtins.open', mock_read), patch(
            'opentf.tools.ctlqualitygate._post', mock_post
        ), patch('requests.post', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ), patch(
            'opentf.tools.ctlqualitygate._fatal', mock_fatal
        ), self.assertRaises(
            SystemExit
        ) as ex:
            _query_qualitygate(UUID_OK, 'fiLepAth', 'nOtmYgAtE', 30)
        mock_fatal.assert_called_once()
        self.assertEqual('Error 422.', mock_fatal.call_args[0][0])
        self.assertEqual('2', str(ex.exception))

    def test_post_qualitygate_file_not_found(self):
        mock_read = mock_open(read_data='')
        mock_read.side_effect = FileNotFoundError('File not found.')
        mock_fnf = MagicMock()
        with patch('builtins.open', mock_read), patch(
            'opentf.tools.ctlqualitygate._file_not_found', mock_fnf
        ):
            _post_qualitygate_definition(UUID_OK, 'fiLepAth', 'nOtmYgAtE', 30)
        mock_fnf.assert_called_once()
        self.assertEqual('fiLepAth', mock_fnf.call_args[0][0])
        self.assertEqual('File not found.', str(mock_fnf.call_args[0][1]))

    def test_post_qualitygate_definition_exception(self):
        mock_read = mock_open(read_data='')
        mock_read.side_effect = ValueError('Some other error.')
        mock_fatal = MagicMock(side_effect=SystemExit())
        with patch('builtins.open', mock_read), patch(
            'opentf.tools.ctlqualitygate._fatal', mock_fatal
        ):
            self.assertRaises(
                SystemExit,
                _post_qualitygate_definition,
                UUID_OK,
                'fiLepAth',
                'nOtmYgAtE',
                30,
            )
        mock_fatal.assert_called_once()
        self.assertIn('Some other error.', mock_fatal.call_args[0][0])

    # get qualitygate with --gl parameters

    def test_get_qualitygate_with_gl_params(self):
        response = {'status': 'NOTEST'}
        mock_qg = MagicMock(return_value=response)
        mock_drtt = MagicMock()
        mock_agp = MagicMock(return_value={'mr': '2'})
        with patch('opentf.tools.ctlqualitygate._query_qualitygate', mock_qg), patch(
            'opentf.tools.ctlqualitygate.dispatch_results_by_target_type', mock_drtt
        ), patch('opentf.tools.ctlqualitygate._add_gl_params', mock_agp), patch(
            'os.environ', {}
        ):
            get_qualitygate('wf_id', 'mode', False, None)
        mock_drtt.assert_called_once_with(
            {'mr': '2', 'server': None, 'project': None}, 'mode', response
        )

    def test_add_gl_params(self):
        params = [
            'get',
            'qualitygate',
            '82',
            '-m',
            'qg',
            '--plugin=gitlab:server=https://gl.gl/',
            '--plugin',
            'gitlab:project=4120831',
            '--plugin',
            'gitlab:mr=42',
            '--plugin',
            'gitlab:cats=true',
        ]
        result = _add_gl_params(params)
        self.assertEqual(result['server'], 'https://gl.gl/')
        self.assertEqual(result['project'], '4120831')
        self.assertEqual(result['mr'], '42')
        self.assertEqual(result['cats'], 'true')
        self.assertNotIn('qg', result)
        self.assertNotIn('qualitygate', result)

    # describe quality gate

    def test_describe_qualitygate_exec_status_success(self):
        qg = {
            'another rule': {
                'result': 'SUCCESS',
                'success_ratio': '105.0%',
                'tests_failed': 0,
                'tests_in_scope': 11,
                'tests_passed': 12,
                'threshold': '104%',
            },
        }
        mock_print = MagicMock()
        mock = MagicMock(return_value={'details': {'rules': qg, 'status': 'SUCCESS'}})
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'builtins.print', mock_print
        ):
            get_qualitygate(UUID_OK, 'mYGate', True)
        self.assertTrue('mode=mYGate' in mock.call_args_list[0][0][1])
        print(mock_print.call_args_list)
        self.assertEqual(mock_print.call_count, 9)
        self.assertIn('mYGate, another rule', mock_print.call_args_list[0][0][0])
        self.assertIn('passed', mock_print.call_args_list[1][0][0])
        self.assertIn('Success ratio:  105.0%', mock_print.call_args_list[6][0][0])

    def test_describe_qualitygate_exec_status_unexpected(self):
        qg = {
            'another rule': {
                'result': 'STATUSQUO',
            }
        }
        mock_print = MagicMock()
        mock = MagicMock(return_value={'details': {'rules': qg, 'status': 'STATUSQUO'}})
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'builtins.print', mock_print
        ):
            self.assertRaises(SystemExit, get_qualitygate, UUID_OK, 'mYGate', True)
        self.assertTrue('mode=mYGate' in mock.call_args_list[0][0][1])
        mock_print.assert_not_called()

    def test_describe_qualitygate_result_unexpected(self):
        qg = {
            'another rule': {
                'result': 'STATUSQUO',
            }
        }
        mock_print = MagicMock()
        mock_error = MagicMock()
        mock = MagicMock(return_value={'details': {'rules': qg, 'status': 'NOTEST'}})
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'builtins.print', mock_print
        ), patch(
            'opentf.tools.ctlqualitygate._error', mock_error
        ):
            get_qualitygate(UUID_OK, 'mYGate', True)
        self.assertIn(
            'Unexpected quality gate result when applying rule `another rule`: STATUSQUO',
            mock_error.call_args[0][0],
        )

    def test_describe_qualitygate_invalid_scope(self):
        qg = {
            'another rule': {
                'result': 'INVALID_SCOPE',
                'scope': 'invalid scope',
                'message': 'invalid scope message',
            }
        }
        mock_print = MagicMock()
        mock = MagicMock(return_value={'details': {'rules': qg, 'status': 'NOTEST'}})
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'builtins.print', mock_print
        ):
            get_qualitygate(UUID_OK, 'mYGate', True)
        self.assertEqual(
            'The quality gate `mYGate` rule `another rule` scope `invalid scope` is invalid.',
            mock_print.call_args_list[1][0][0],
        )

    # qualitygate cmd

    def test_qualitygate_cmd_1(self):
        args = ['', 'get', 'qualitygate', UUID_OK]
        mock = MagicMock(return_value={'code': 200, 'details': {'status': 'NOTEST'}})
        mock2 = MagicMock()
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'opentf.tools.ctlqualitygate.read_configuration', mock2
        ):
            qualitygate_cmd()
        mock.assert_called_once()
        self.assertEqual(mock.call_args[0][0], 'http://aaaa')
        mock2.assert_called_once()

    def test_qualitygate_cmd_2(self):
        args = ['', 'get', 'qualitygate', UUID_OK, '--mode=yada']
        mock = MagicMock(return_value={'code': 422, 'message': 'bad mode'})
        mock2 = MagicMock()
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'opentf.tools.ctlqualitygate.read_configuration', mock2
        ):
            self.assertRaises(SystemExit, qualitygate_cmd)

    def test_qualitygate_cmd_3(self):
        args = ['', 'get', 'qualitygate', UUID_BAD]
        mock = MagicMock(return_value={'code': 404})
        mock2 = MagicMock()
        with patch('opentf.tools.ctlworkflows._get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'opentf.tools.ctlqualitygate.read_configuration', mock2
        ):
            self.assertRaises(SystemExit, qualitygate_cmd)

    def test_qualitygate_cmd_describe_qualitygate_ok(self):
        args = ['', 'describe', 'qualitygate', UUID_OK]
        mock = MagicMock(return_value={'code': 200, 'details': {'status': 'NOTEST'}})
        mock2 = MagicMock()
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'opentf.tools.ctlqualitygate.read_configuration', mock2
        ):
            qualitygate_cmd()
        mock.assert_called_once()
        self.assertEqual(mock.call_args[0][0], 'http://aaaa')
        mock2.assert_called_once()

    def test_qualitygate_cmd_ko(self):
        args = ['', 'get', 'qualitygat', UUID_BAD]
        mock = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctlqualitygate.get_qualitygate', mock
        ):
            self.assertRaises(SystemExit, qualitygate_cmd)
        mock.assert_not_called()

    def test_qualitygate_cmd_empty_gl_params(self):
        args = [
            '',
            'get',
            'qualitygate',
            UUID_OK,
            '--plugin',
            'gitlab:keep-history=true',
            '--plugin',
            'gitlab:label=',
        ]
        mock = MagicMock(return_value={'code': 200, 'details': {'status': 'NOTEST'}})
        mock2 = MagicMock()
        mock_drtt = MagicMock()
        mock_warning = MagicMock()
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'opentf.tools.ctlqualitygate.read_configuration', mock2
        ), patch(
            'opentf.tools.ctlqualitygate.dispatch_results_by_target_type', mock_drtt
        ), patch(
            'opentf.tools.ctlqualitygate._warning', mock_warning
        ):
            qualitygate_cmd()
        mock.assert_called_once()
        self.assertEqual(mock.call_args[0][0], 'http://aaaa')
        mock2.assert_called_once()
        mock_warning.assert_called_once_with(
            'The command-line option `gitlab:%s` value is empty.', 'label'
        )

    def test_qualitygate_cmd_gl_params_from_env(self):
        args = [
            '',
            'get',
            'qualitygate',
            UUID_OK,
            '--plugin',
            'gitlab:keep-history=true',
        ]
        mock = MagicMock(return_value={'code': 200, 'details': {'status': 'NOTEST'}})
        mock2 = MagicMock()
        mock_drtt = MagicMock()
        mock_warning = MagicMock()
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'opentf.tools.ctlqualitygate.read_configuration', mock2
        ), patch(
            'opentf.tools.ctlqualitygate.dispatch_results_by_target_type', mock_drtt
        ), patch(
            'opentf.tools.ctlqualitygate._warning', mock_warning
        ), patch(
            'os.environ',
            {
                'CI_SERVER_URL': 'uRl',
                'CI_MERGE_REQUEST_PROJECT_ID': 920392002,
                'CI_MERGE_REQUEST_IID': 999,
            },
        ):
            qualitygate_cmd()
        mock.assert_called_once()
        self.assertEqual(mock.call_args[0][0], 'http://aaaa')
        mock2.assert_called_once()
        mock_warning.assert_not_called()
        mock_drtt.assert_called_once_with(
            {'keep_history': 'true', 'server': 'uRl', 'project': 920392002, 'mr': 999},
            'strict',
            {'status': 'NOTEST'},
        )

    def test_qualitygate_cmd_1_legacy(self):
        args = ['', 'get', 'qualitygate', UUID_OK]
        mock = MagicMock(return_value={'code': 200, 'details': {'status': 'NOTEST'}})
        mock2 = MagicMock()
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'opentf.tools.ctlqualitygate.read_configuration', mock2
        ):
            qualitygate_cmd()
        mock.assert_called_once()
        self.assertEqual(mock.call_args[0][0], 'http://aaaa')
        mock2.assert_called_once()

    def test_qualitygate_cmd_2_legacy(self):
        args = ['', 'get', 'qualitygate', UUID_OK, '--mode=yada']
        mock = MagicMock(return_value={'code': 422, 'message': 'bad mode'})
        mock2 = MagicMock()
        with patch('opentf.tools.ctlqualitygate._get_json', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'opentf.tools.ctlqualitygate.read_configuration', mock2
        ):
            self.assertRaises(SystemExit, qualitygate_cmd)

    def test_qualitygate_cmd_3_legacy(self):
        args = ['', 'get', 'qualitygate', UUID_BAD]
        mock = MagicMock(return_value={'code': 404})
        mock2 = MagicMock()
        with patch('opentf.tools.ctlworkflows._get', mock), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'opentf.tools.ctlqualitygate.read_configuration', mock2
        ):
            self.assertRaises(SystemExit, qualitygate_cmd)

    # print qualitygate help

    def test_print_get_qualitygate_help(self):
        args = ['', 'get', 'qualitygate']
        mock = MagicMock()
        with patch('builtins.print', mock):
            print_qualitygate_help(args)
        self.assertTrue(mock.call_count > 0)

    def test_print_describe_qualitygate_help(self):
        args = ['', 'describe', 'qualitygate']
        mock = MagicMock()
        with patch('builtins.print', mock):
            print_qualitygate_help(args)
        self.assertTrue(mock.call_count > 0)

    def test_print_get_qualitygate_help_ko(self):
        args = ['', 'get', 'qualitygat']
        mock = MagicMock()
        with patch('builtins.print', mock):
            self.assertRaises(SystemExit, print_qualitygate_help, args)
        self.assertTrue(mock.call_count == 0)
